# georoc-frontend-helm

This repository contains the helm chart for the digis/georoc search frontend project.

## Usage

This repo contains a base-helmchart and multiple `values.yaml` variants to manage different releases.
The structure is related to the suggestions in [this article](https://codefresh.io/blog/how-to-model-your-gitops-environments-and-promote-releases-between-them/).

To create a new release, combine the different values files, e.g. `helm template georoc-frontend/ --values common/values-common.yaml --values envs/values-staging.yaml` for a staging release.
